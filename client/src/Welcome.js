import React from 'react';
import './welcome.css';

const Welcome = () => (
    <div className="welcome">
        <header className="welcome__top">
        <Logo />
        </header>
        <section className="welcome__main">
            <div className="main__content">
                <h1>
                    Welcome to the <strong>baselisc API</strong>!
                </h1>
                <div className="main__before-starting">
                    <p>
                        This container hosts our <b>Progressive Web App</b>{' '}
                        ({'http:' === document.location.protocol ? <a href="https://localhost">HTTPS</a> : <a href="http://localhost">HTTP</a>}).
                        We created a API and generated a PWA:
                    </p>
                </div>
                <div className="main__other">
                    <h2>Available services:</h2>
                    <div className="other__bloc">
                        <div className="other__circle">
                            <Api />
                        </div>
                        <div className="other__content">
                            <h3>API</h3>
                            <ButtonsGroup
                                httpLink="http://localhost:8080"
                                httpsLink="https://localhost:8443"
                            />
                            <h3>Cached API</h3>
                            <ButtonsGroup
                                httpLink="http://localhost:8081"
                                httpsLink="https://localhost:8444"
                            />
                        </div>
                    </div>
                    <div className="other__bloc">
                        <div className="other__circle">
                            <Admin />
                        </div>
                        <div className="other__content">
                            <h3>Admin</h3>
                            <ButtonsGroup
                                httpLink="http://localhost:81"
                                httpsLink="https://localhost:444"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
);

const ButtonsGroup = ({ httpLink, httpsLink }) => (
    <div className="buttons__group">
        <a
            target="_blank"
            rel="noopener noreferrer"
            href={httpLink}
            className="other__button"
        >
            http
        </a>
        <div className="buttons__or" />
        <a
            target="_blank"
            rel="noopener noreferrer"
            href={httpsLink}
            className="other__button"
        >
            https
        </a>
    </div>
);

export default Welcome;

const Logo = ({ className }) => (
    <span class="log___object"></span>
);

const Api = () => (
    <span class="api___object"></span>
);

const Admin = () => (
    <span class="admin___object"></span>
);