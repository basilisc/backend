# THE BACKEND

### How to get it up and running

* `docker-compose up -d # Running in detached mode`
* `docker-compose exec php bin/console doctrine:schema:update --force # To Update the database schema`

With the Command `docker-compose exec php bin/console` you get Access to the Symfony 4 Console Component where you can Access Doctrine or other Development Tools.

For more Information about the Commandline capabilities checkout the Symfony Documentation at: https://symfony.com/doc/current/console.html

### How to debug/gather infos

* `docker-compose logs -f # follow the logs`