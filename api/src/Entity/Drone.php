<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A basic entity for every unique drone
 *
 * @ApiResource
 * @ORM\Entity
 */
class Drone
{
    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @var string The Mac-Adress of the drone
	 *
	 * @ORM\Column
	 * @Assert\NotBlank
	 */
	private $macAdress = '';

	/**
	 * @var string The SSID of the drone
	 *
	 * @ORM\Column
	 * @Assert\NotBlank
	 */
	private $ssid = '';

    public function getId(): int
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getMacAdress()
	{
		return $this->macAdress;
	}

	/**
	 * @param string $macAdress
	 */
	public function setMacAdress($macAdress)
	{
		$this->macAdress = $macAdress;
	}

	/**
	 * @return string
	 */
	public function getSsid()
	{
		return $this->ssid;
	}

	/**
	 * @param string $ssid
	 */
	public function setSsid($ssid)
	{
		$this->ssid = $ssid;
	}
}
