<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The Path of drone during a DroneSighting
 *
 * @ApiResource
 * @ORM\Entity
 */
class DronePath
{
    /**
     * @var int The sighting Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DroneSighting The sighting connected to the Path object
     *
     * @ORM\ManyToOne(targetEntity="DroneSighting")
	 * @ORM\JoinColumn(name="drone_sighting_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
	private $droneSighting;

	/**
	 * @var \DateTimeImmutable The time a drone was detected
	 *
	 * @ORM\Column(type="datetime_immutable")
	 */
	private $date;

	/**
	 * @var int The Signal strength of the drone WiFi in dBm
	 *
	 * @ORM\Column(type="integer")
	 * @Assert\NotBlank
	 */
	private $signalStrength;

	/**
	 * @var int The direction of the Signal in degrees
	 *
	 * @ORM\Column(type="integer")
	 * @Assert\NotBlank
	 */
	private $direction;

	public function __construct()
	{
		$this->date = new \DateTimeImmutable();
	}

    public function getId(): int
    {
        return $this->id;
    }

	/**
	 * @return \DateTimeImmutable
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param \DateTimeImmutable $date
	 */
	public function setDate(\DateTimeImmutable $date)
	{
		$this->date = $date;
	}

	/**
	 * @return DroneSighting
	 */
	public function getDroneSighting()
	{
		return $this->droneSighting;
	}

	/**
	 * @param DroneSighting $droneSighting
	 */
	public function setDroneSighting($droneSighting)
	{
		$this->droneSighting = $droneSighting;
	}

	/**
	 * @return int
	 */
	public function getSignalStrength()
	{
		return $this->signalStrength;
	}

	/**
	 * @param int $signalStrength
	 */
	public function setSignalStrength($signalStrength)
	{
		$this->signalStrength = $signalStrength;
	}

	/**
	 * @return int
	 */
	public function getDirection()
	{
		return $this->direction;
	}

	/**
	 * @param int $direction
	 */
	public function setDirection($direction)
	{
		$this->direction = $direction;
	}
}
