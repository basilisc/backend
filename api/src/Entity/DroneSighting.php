<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A sighting of a certain drone
 *
 * @ApiResource
 * @ORM\Entity
 */
class DroneSighting
{
    /**
     * @var int The sighting Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Drone The Drone that was sighted
     *
     * @ORM\ManyToOne(targetEntity="Drone")
	 * @ORM\JoinColumn(name="drone_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
	private $drone;

	/**
	 * @var \DateTimeImmutable The first time a drone was detected and a new DroneSighting was created
	 *
	 * @ORM\Column(type="datetime_immutable")
	 */
	private $creationDate;

	/**
	 * @var float The longitude of the base-plate position for this drone sighting
	 *
	 * @ORM\Column(type="float")
	 */
	private $platePositionLong;

	/**
	 * @var float The latitude of the base-plate position for this drone sighting
	 *
	 * @ORM\Column(type="float")
	 */
	private $platePositionLat;

	public function __construct()
	{
		$this->creationDate = new \DateTimeImmutable();
	}

    public function getId(): int
    {
        return $this->id;
    }

	/**
	 * @return Drone
	 */
	public function getDrone()
	{
		return $this->drone;
	}

	/**
	 * @param Drone $drone
	 */
	public function setDrone(Drone $drone)
	{
		$this->drone = $drone;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getCreationDate()
	{
		return $this->creationDate;
	}

	/**
	 * @param \DateTimeImmutable $creationDate
	 */
	public function setCreationDate($creationDate)
	{
		$this->creationDate = $creationDate;
	}

	/**
	 * @return float
	 */
	public function getPlatePositionLong()
	{
		return $this->platePositionLong;
	}

	/**
	 * @param float $platePositionLong
	 */
	public function setPlatePositionLong($platePositionLong)
	{
		$this->platePositionLong = $platePositionLong;
	}

	/**
	 * @return float
	 */
	public function getPlatePositionLat()
	{
		return $this->platePositionLat;
	}

	/**
	 * @param float $platePositionLat
	 */
	public function setPlatePositionLat($platePositionLat)
	{
		$this->platePositionLat = $platePositionLat;
	}
}
